{ pkgs ? import <nixpkgs> {}, lib ? pkgs.lib }:

with lib;

let
  neededLibraries = with pkgs; [
    readline
  ];

  pyPackages = with pkgs.python38Packages; [
    pkgs.python38
    setuptools
    virtualenv
    pip

    pyyaml
    passlib
    diceware
  ];

in
pkgs.mkShell {
  buildInputs = flatten [
    pyPackages
    neededLibraries
  ];

  "LD_LIBRARY_PATH" = makeLibraryPath neededLibraries;
}
