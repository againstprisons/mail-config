from types import SimpleNamespace
from passlib.hash import sha512_crypt

from ..cli import argument, subcommand




def generate_passphrase():
    from diceware import get_passphrase

    options = SimpleNamespace(infile=None, wordlist='en_eff', randomsource='system', num=6, caps=False, delimiter='-', specials=0)
    return get_passphrase(options)


@subcommand()
def pwgen(args):
    """Generate and hash a secure passphrase for a mail account.
    """

    password = generate_passphrase()
    print(f"Password:\n\t{password}")

    hash = "{SHA512-CRYPT}" + sha512_crypt.hash(password, rounds=5000)
    print(f"Hash:\n\t{hash}")
