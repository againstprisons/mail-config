import logging

from . import __version__
from pathlib import Path
from argparse import ArgumentParser


cli = ArgumentParser()
cli.add_argument('--version', action='version', version=f"%(prog)s {__version__}")
cli.add_argument('-c', '--config', action='store', help='Path to configuration files', default='./config')
subparsers = cli.add_subparsers(dest='subcommand')


def argument(*name_or_flags, **kwargs):
    """Convenience function to properly format arguments to pass to the
    subcommand decorator.
    """

    return (list(name_or_flags), kwargs)


def subcommand(args=[], parent=subparsers):
    """Decorator to define a new subcommand.

    Usage example::
        @subcommand([argument('-d', help='Enable debug mode', action='store_true')])
        def subcommand_func(args):
            print(args)
    """

    def decorator(func):
        parser = parent.add_parser(func.__name__, description=func.__doc__)
        for arg in args:
            parser.add_argument(*arg[0], **arg[1])

        parser.set_defaults(func=func)

    return decorator


def main():
    """Main entrypoint function, dispatching subcommands.
    """

    from . import commands
    args = cli.parse_args()

    if args.subcommand is None:
        cli.print_help()
        return 1
    else:
        return args.func(args)
