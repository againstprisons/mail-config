import yaml

from pathlib import Path


class MailConfiguration:
    def __init__(self, path):
        self.path = Path(path)
        if not self.path.is_dir():
            raise ArgumentError(f"Mail configuration path {path!r} does not exist")

        # TODO: maybe allow other file formats?
        self.cfg_base = self._load_config('config.yml')
        self.cfg_users = self._load_config('users.yml')
        self.cfg_roles = self._load_config('roles.yml')


    def _load_config(self, filename, base_path=None):
        base_path = Path(base_path or self.path)
        file_path = base_path / filename
        if not file_path.is_file():
            raise ArgumentError(f"Configuration file {file_path!r} does not exist")

        with file_path.open() as fh:
            if file_path.suffix in ['.yml', '.yaml']:
                return yaml.safe_load(fh)

            else:
                raise ArgumentError(f"Configuration file {file_path!r} is of an unsupported type")

    @property
    def primary_domain(self):
        return self.cfg_base['domains']['default'][0]

    @property
    def alias_domains(self):
        return self.cfg_base['domains']['default'][1:]

    @property
    def roles(self):
        roles = self.cfg_roles

        # Allow empty role definitions
        for role_name in roles.keys():
            if roles[role_name] is None:
                roles[role_name] = {}

        # Add empty role for each user that doesn't already have a role
        for (user_name, user_data) in self.cfg_users.items():
            if user_name not in roles:
                roles[user_name] = {}

        return roles

    @property
    def users(self):
        users = self.cfg_users

        # Allow empty user definitions
        for user_name in users.keys():
            if users[user_name] is None:
                users[user_name] = {}

        return users
