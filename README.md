# MailConfig

A config generator designed for [docker-mailserver][], that can (probably) be
used for other Postfix installs.


## Usage

Install with `pip install -U .` from the root of this repository.

See the example configuration in the `example-configs` directory for how your
configuration should be structured.

To generate the output Postfix configuration files, run:

```shell
% MailConfig --config /path/to/input generate --output /path/to/output
```


[docker-mailserver]: https://github.com/docker-mailserver/docker-mailserver


<hr>

#### License

<sup>
Licensed under either of
<a href="http://www.apache.org/licenses/LICENSE-2.0">Apache License, Version
2.0</a> or <a href="http://opensource.org/licenses/MIT">MIT license</a>, at
your option.
</sup>

<br>

<sub>
Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall
be dual licensed as above, without any additional terms or conditions.
</sub>
